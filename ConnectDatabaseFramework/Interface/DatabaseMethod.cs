﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ConnectDatabaseFramework.Interface
{
    interface DatabaseMethod
    {
        public void SetConnectionString(string strConn);
        public IDbConnection CreateConnection();
        public List<string> GetListTableNames(string databaseName, IDbConnection dbConnection);
    }
}
