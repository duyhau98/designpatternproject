﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ConnectDatabaseFramework
{
    public class SQLConnect : DatabaseConnect
    {


        public override IDbConnection CreateConnection()
        {

                    return new SqlConnection(this.ConnectionString);


        }

        public override List<string> GetListTableNames(string databaseName, IDbConnection dbConnection)
        {
            string SqlQuery = "SELECT name FROM " + databaseName + ".sys.Tables;";
            var dbCommand = new SqlCommand(SqlQuery, (SqlConnection) dbConnection);
            dbConnection.Open();
            SqlDataReader sqlDataReader = dbCommand.ExecuteReader();
            List<string> result = new List<string>();
            while (sqlDataReader.Read())
            {
                result.Add(sqlDataReader[0].ToString());
                Console.WriteLine(sqlDataReader[0]);
            }
            sqlDataReader.Close();
            dbConnection.Close();
            return result;
        }
    }
}
