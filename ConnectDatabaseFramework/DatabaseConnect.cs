﻿using ConnectDatabaseFramework.Interface;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ConnectDatabaseFramework
{
    public class DatabaseConnect : DatabaseMethod
    {
        public  string ConnectionString { get; set; }


        public  void SetConnectionString(string strConn)
        {
            ConnectionString = strConn;
        }

        public virtual  IDbConnection CreateConnection()
        {

            return null;


        }
        public  DatabaseConnect GetDatabaseTypes(string db)
        {
            switch (db)
            {
                default:
                case "Sql":
                    return new SQLConnect();
                case "MySql":
                    return new MySQLConnect();
            }
        }

        public  IDbDataAdapter GetDataAdapter(string db)
        {
            switch (db)
            {
                default:
                case "Sql":
                    return new SqlDataAdapter();
                case "MySql":
                    return new MySqlDataAdapter();
            }
        }

        public static DatabaseConnect GetCommand(string db)
        {
            switch (db)
            {
                default:
                case "MsSql":
                    return new SQLConnect();
                case "MySql":
                    return new MySQLConnect();
            }
        }

        public virtual List<string> GetListTableNames(string databaseName, IDbConnection dbConnection)
        {
            return null;
        }
    }
}
